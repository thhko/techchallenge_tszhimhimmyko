﻿using EasyBet.API.Services.Models;
using System;

namespace EasyBet.API.Services
{
    public interface ICustomerService
    {
        ServiceResponse GetCustomers();
    }
}
