﻿using EasyBet.API.Services.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace EasyBet.API.Services.Cloud
{
    public class AzureCustomerService : AzureBaseService, ICustomerService
    {
        public ServiceResponse GetCustomers()
        {
            Uri targetUri = new Uri($"{_serviceEndPoint}/api/GetCustomers?name={_serviceEndPointNameQueryParam}");
            bool apiCallSuccess = _httpClient.Get(targetUri, _ttl, out string response);

            ServiceResponse serviceCallResponse = new ServiceResponse();

            if (apiCallSuccess)
                serviceCallResponse.SuccessResponse(response);
            else
                serviceCallResponse.FailureResponse(response);

            return serviceCallResponse;
        }
    }
}
