﻿using EasyBet.API.Services.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Text;
using System.Xml.Linq;

namespace EasyBet.API.Services.Cloud
{
    public class AzureBetService : AzureBaseService, IBetService
    {
        public ServiceResponse GetBets()
        {
            Uri targetUri = new Uri($"{_serviceEndPoint}/api/GetBetsV2?name={_serviceEndPointNameQueryParam}");
            bool apiCallSuccess = _httpClient.Get(targetUri, _ttl, out string response);

            ServiceResponse serviceCallResponse = new ServiceResponse();

            if (apiCallSuccess)
                serviceCallResponse.SuccessResponse(response);
            else
                serviceCallResponse.FailureResponse(response);

            return serviceCallResponse;
        }
    }
}
