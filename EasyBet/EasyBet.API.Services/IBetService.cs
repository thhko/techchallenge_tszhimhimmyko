﻿using EasyBet.API.Services.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace EasyBet.API.Services
{
    public interface IBetService
    {
        ServiceResponse GetBets();
    }
}
