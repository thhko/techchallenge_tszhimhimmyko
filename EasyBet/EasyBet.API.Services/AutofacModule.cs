﻿using Autofac;
using EasyBet.API.Services.Cloud;
using System;
using System.Collections.Generic;
using System.Text;

namespace EasyBet.API.Services
{
    public class AutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<AzureBetService>().AsImplementedInterfaces();
            builder.RegisterType<AzureCustomerService>().AsImplementedInterfaces();
            builder.RegisterType<AzureRaceService>().AsImplementedInterfaces();
        }
    }
}
