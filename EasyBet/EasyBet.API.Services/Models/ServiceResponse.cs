﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EasyBet.API.Services.Models
{
    public class ServiceResponse
    {
        private bool _success;
        private string _responseData;
        private string _errorMessage;

        public bool Success => _success;
        public string ResponseData => _responseData;
        public string ErrorMessage => _errorMessage;

        public void SuccessResponse(string responseData)
        {
            _success = true;
            _responseData = responseData;
        }

        public void FailureResponse(string errorMessage)
        {
            _success = false;
            _errorMessage = errorMessage;
        }

    }
}
