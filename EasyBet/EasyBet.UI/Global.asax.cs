﻿using Autofac;
using Autofac.Integration.Mvc;
using EasyBet.UI.Logics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace EasyBet.UI
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            RegisterAutoFacContainer();
        }

        private void RegisterAutoFacContainer()
        {
            var builder = new ContainerBuilder();

            RegisterBCs(builder);
            RegisterConnectors(builder);

            builder.RegisterControllers(typeof(MvcApplication).Assembly);

            var ioc = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(ioc));
        }

        private void RegisterBCs(ContainerBuilder builder)
        {
            builder.RegisterType<MyRaceDayLogics>().AsImplementedInterfaces();
        }

        private void RegisterConnectors(ContainerBuilder builder)
        {
            System.Reflection.Assembly connectorAssembly = System.Reflection.Assembly.Load("EasyBet.UI.Connector");
            builder.RegisterAssemblyModules(connectorAssembly);
        }
    }
}
