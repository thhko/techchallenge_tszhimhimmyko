﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace EasyBet.UI.Helpers
{
    public static class FrontendHelper
    {
        public static MvcHtmlString GetEnumDescription<T>(T enumValue) where T : struct
        {
            string displayText;

            FieldInfo fi = enumValue.GetType().GetField(enumValue.ToString());
            try
            {
                DescriptionAttribute[] descAttr = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
                displayText = descAttr != null && descAttr.Length > 0 ? descAttr[0].Description : enumValue.ToString();
            }
            catch
            {
                displayText = enumValue.ToString();
            }
            return new MvcHtmlString(displayText);
        }
    }
}