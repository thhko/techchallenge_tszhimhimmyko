﻿using EasyBet.UI.Logics;
using EasyBet.UI.Models;
using EasyBet.UI.Models.Racers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EasyBet.UI.Controllers
{
    public class HomeController : Controller
    {
        private IRaceDayLogics _raceDayLogics;

        public HomeController(IRaceDayLogics raceDayLogics)
        {
            _raceDayLogics = raceDayLogics;
        }

        public ActionResult Index()
        {
            RaceDayBoardViewModel model = _raceDayLogics.GetCurrentRaces();

            return View(model);
        }
    }
}