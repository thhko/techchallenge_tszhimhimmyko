﻿using EasyBet.UI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EasyBet.UI.Logics
{
    public interface IRaceDayLogics
    {
        RaceDayBoardViewModel GetCurrentRaces();
    }
}