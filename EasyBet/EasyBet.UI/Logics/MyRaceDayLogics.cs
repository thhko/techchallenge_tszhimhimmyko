﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EasyBet.UI.Connector;

namespace EasyBet.UI.Logics
{
    public class MyRaceDayLogics : BaseRaceDayLogics
    {
        public MyRaceDayLogics(IRaceConnector raceConnector, IBetConnector betConnector) 
            : base(raceConnector, betConnector)
        {
        }

        //custom logic implementation should go here
    }
}