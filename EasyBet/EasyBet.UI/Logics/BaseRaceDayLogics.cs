﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using EasyBet.UI.Connector;
using EasyBet.UI.Connector.Models;
using EasyBet.UI.Models;
using EasyBet.UI.Models.Racers;
using Newtonsoft.Json;

namespace EasyBet.UI.Logics
{
    /// <summary>
    /// Base implementation of the race day logic
    /// </summary>
    public class BaseRaceDayLogics : IRaceDayLogics
    {
        private IRaceConnector _raceConnector;
        private IBetConnector _betConnector;

        public BaseRaceDayLogics(IRaceConnector raceConnector, IBetConnector betConnector)
        {
            _raceConnector = raceConnector;
            _betConnector = betConnector;
        }

        public virtual RaceDayBoardViewModel GetCurrentRaces()
        {
            List<Race<IRacer>> allRaces = GetRacesDetailsFromAPI();

            return new RaceDayBoardViewModel(allRaces);
        }

        #region Private Method
        private List<Race<IRacer>> GetRacesDetailsFromAPI()
        {
            ApiResponse racesDataResponse = _raceConnector.GetRaces();
            ApiResponse betsDataResponse = _betConnector.GetBets();
            
            if(racesDataResponse.Success && betsDataResponse.Success)
            {
                List<RaceData> racesData = ExtractRaceDataFromResponse(racesDataResponse.ResponseData);
                List<BetData> betsData = ExtractBetDataFromResponse(betsDataResponse.ResponseData);

                //Process data returned
                List<Race<IRacer>> processedData = ProcessApiData(racesData, betsData);
                //return model
                return processedData;
            }
            else
                return null;
        }

        private List<Race<IRacer>> ProcessApiData(List<RaceData> racesData, List<BetData> betsData)
        {
            List<Race<IRacer>> raceResults = new List<Race<IRacer>>();

            foreach (RaceData race in racesData)
            {
                Race<IRacer> horseRace = new Race<IRacer>(
                    //get the status of the race
                    (RaceStatus) Enum.Parse(typeof(RaceStatus), race.status, true),
                    //get the horses information
                    race.horses.Select(racer =>
                        new Horse(
                            racer.name,
                            betsData.Where(bet => bet.raceId == race.id && bet.horseId == racer.id).Count(),
                            betsData.Where(bet => bet.raceId == race.id && bet.horseId == racer.id).Select(bet=>bet.stake).Sum(),
                            racer.odds
                            ) as IRacer).ToList()
                    );
                raceResults.Add(horseRace);
            }

            return raceResults;
        }

        private List<RaceData> ExtractRaceDataFromResponse(string jsonResponse)
        {
            return JsonConvert.DeserializeObject<List<RaceData>>(jsonResponse);
        }

        private List<BetData> ExtractBetDataFromResponse(string jsonResponse)
        {
            return JsonConvert.DeserializeObject<List<BetData>>(jsonResponse);
        }

        #endregion
    }
}