﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EasyBet.UI.Models
{
    /// <summary>
    /// Class for deserialization of API data returned for bets
    /// </summary>
    public class BetData
    {
        public long customerId { get; set; }
        public long horseId { get; set; }
        public long raceId { get; set; }
        public decimal stake { get; set; }
    }
}