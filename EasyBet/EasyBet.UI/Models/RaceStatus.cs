﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace EasyBet.UI.Models
{
    public enum RaceStatus
    {
        [Description("Pending")]
        Pending,
        [Description("In Progress")]
        In_Progress,
        [Description("Completed")]
        Completed
    }
}