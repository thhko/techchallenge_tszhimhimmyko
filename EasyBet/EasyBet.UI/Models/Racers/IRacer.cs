﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EasyBet.UI.Models.Racers
{
    public interface IRacer
    {
        string Name { get; }
        long BetsCount { get; }
        decimal Winner_Payout { get; }
        decimal TotalBetsAmout { get; }
        decimal Odds { get; }

    }
}