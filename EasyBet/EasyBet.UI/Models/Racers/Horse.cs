﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EasyBet.UI.Models.Racers
{
    public class Horse : IRacer
    {
        public string Name { get => _name; }
        public long BetsCount { get => _betsCount; }
        public decimal TotalBetsAmout { get => _totalBetsAmount; }
        public decimal Odds { get => _odds; }
        public decimal Winner_Payout { get => CalculatePayout(); }

        private string _name;
        private long _betsCount;
        private decimal _totalBetsAmount;
        private decimal _odds;

        public Horse(string name, long betsCount, decimal totalBetsAmount, decimal odds)
        {
            _name = name;
            _betsCount = betsCount;
            _totalBetsAmount = totalBetsAmount;
            _odds = odds;
        }

        private decimal CalculatePayout()
        {
            return _totalBetsAmount * _odds;
        }
    }
}