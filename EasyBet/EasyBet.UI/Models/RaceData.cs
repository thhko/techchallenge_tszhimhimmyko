﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EasyBet.UI.Models
{
    /// <summary>
    /// Class for deserialization of API data returned for races
    /// </summary>
    public class RaceData
    {
        public long id { get; set; }
        public string name { get; set; }
        public DateTime start { get; set; }
        public string status { get; set; }

        public List<HorseData> horses { get; set; }
    }
}