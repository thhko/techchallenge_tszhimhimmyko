﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EasyBet.UI.Models
{
    /// <summary>
    /// Class for deserialization of API data returned for horses
    /// </summary>
    public class HorseData
    {
        public long id { get; set; }
        public string name { get; set; }
        public decimal odds { get; set; }
    }
}