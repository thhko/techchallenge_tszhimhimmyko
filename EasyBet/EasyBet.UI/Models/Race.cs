﻿using EasyBet.UI.Models.Racers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EasyBet.UI.Models
{
    public class Race<T> where T : IRacer
    {
        public RaceStatus CurrentStatus { get => _currentStatus; }
        public decimal TotalBetsAmount { get => CalculateTotalBetsAmount(); }
        public List<T> Racers { get => _racers; }

        private RaceStatus _currentStatus;
        private List<T> _racers;

        public Race(RaceStatus status, List<T> racers)
        {
            _currentStatus = status;
            _racers = racers;
        }

        private decimal CalculateTotalBetsAmount()
        {
            return Racers.Select(racer => racer.TotalBetsAmout).Sum();
        }
    }
}