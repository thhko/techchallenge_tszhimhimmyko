﻿using EasyBet.UI.Models.Racers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EasyBet.UI.Models
{
    public class RaceDayBoardViewModel
    {
        public List<Race<IRacer>> Races { get => _races; }

        private List<Race<IRacer>> _races;

        public RaceDayBoardViewModel(List<Race<IRacer>> races)
        {
            _races = races;
        }
    }

}