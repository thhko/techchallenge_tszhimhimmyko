﻿using EasyBet.API.Models;
using EasyBet.API.Services;
using EasyBet.API.Services.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;

namespace EasyBet.API.Logics
{
    public class CustomerLogics
    {
        ICustomerService _customerService;
        IBetService _betService;

        public CustomerLogics(ICustomerService customerService, IBetService betService)
        {
            _customerService = customerService;
            _betService = betService;
        }

        public dynamic GetCustomers()
        {
            return _customerService.GetCustomers();
        }

        public dynamic GetCustomersStatements()
        {
            ServiceResponse customersResponse = _customerService.GetCustomers();
            ServiceResponse betsResponse = _betService.GetBets();

            if(customersResponse.Success && betsResponse.Success )
            {
                dynamic customerData = ExtractJsonDataFromResponse(customersResponse.ResponseData);
                dynamic betsData = ExtractJsonDataFromResponse(betsResponse.ResponseData);

                CustomerStatementSummary summary = CreateCustomerStatementSummary(customerData, betsData);
                return new { Success = true, ResponseData = summary };
            }

            return new { Success = false, ErrorMessage = "Unabled to process request. Please try again." };
        }

        private CustomerStatementSummary CreateCustomerStatementSummary(dynamic customerData, dynamic betsData)
        {
            CustomerStatementSummary summary = new CustomerStatementSummary();

            foreach (var customer in customerData)
            {
                CustomerStatement customerStatement = GetCustomerStatement(customer, betsData);
                summary.AddCustomerStatement(customerStatement);
            }

            return summary;
        }

        private static CustomerStatement GetCustomerStatement(dynamic customer, dynamic betsData)
        {
            CustomerStatement customerStatement = new CustomerStatement(customer.id, customer.name);
            foreach (var customerBet in betsData)
            {
                if (customerBet.customerId == customer.id)
                    customerStatement.AddBet((decimal)customerBet.stake);
            }

            return customerStatement;
        }

        private dynamic ExtractJsonDataFromResponse(string jsonData)
        {
            return JsonConvert.DeserializeObject<List<ExpandoObject>>(jsonData);
        }
    }
}
