﻿using EasyBet.API.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EasyBet.API.Logics
{
    public class BetLogics
    {
        private IBetService _betService;

        public BetLogics(IBetService betService)
        {
            _betService = betService;
        }

        public dynamic GetBets()
        {
            dynamic bets = _betService.GetBets();
            return bets;
        }
    }
}
