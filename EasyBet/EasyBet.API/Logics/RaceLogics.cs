﻿using EasyBet.API.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EasyBet.API.Logics
{
    public class RaceLogics
    {
        private IRaceService _raceService;

        public RaceLogics(IRaceService raceService)
        {
            _raceService = raceService;
        }

        public dynamic GetRaces()
        { 
            return _raceService.GetRaces();
        }

    }
}
