﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EasyBet.API.Configurations;
using EasyBet.API.Logics;
using Microsoft.AspNetCore.Mvc;

namespace EasyBet.API.Controllers
{
    [Route(ApiRouteConfig.ControllerRoute)]
    [Produces(ApiRouteConfig.ControllerProduces)]
    public class CustomerController : Controller
    {
        private CustomerLogics _customerLogics;

        public CustomerController(CustomerLogics customerLogics)
        {
            _customerLogics = customerLogics;
        }

        [HttpGet]
        public object GetAllCustomers()
        {
            return _customerLogics.GetCustomers();
        }

    }
}
