﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EasyBet.API.Configurations;
using EasyBet.API.Logics;
using EasyBet.API.Services;
using EasyBet.API.Services.Cloud;
using Microsoft.AspNetCore.Mvc;

namespace EasyBet.API.Controllers
{
    [Route(ApiRouteConfig.ControllerRoute)]
    [Produces(ApiRouteConfig.ControllerProduces)]
    public class BetController : Controller
    {
        BetLogics _betLogic;

        public BetController(BetLogics betLogics)
        {
            _betLogic = betLogics;
        }

        [HttpGet]
        public dynamic GetAllBets()
        {
            return _betLogic.GetBets();
        }

    }
}
