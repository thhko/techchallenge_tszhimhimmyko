﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EasyBet.API.Configurations;
using EasyBet.API.Logics;
using Microsoft.AspNetCore.Mvc;

namespace EasyBet.API.Controllers
{
    [Route(ApiRouteConfig.ControllerRoute)]
    [Produces(ApiRouteConfig.ControllerProduces)]
    public class CustomerStatementController : Controller
    {
        private CustomerLogics _customerLogics;

        public CustomerStatementController(CustomerLogics customerLogics)
        {
            _customerLogics = customerLogics;
        }

        [HttpGet]
        public object GetAllCustomerStatements()
        {
            return _customerLogics.GetCustomersStatements();
        }

    }
}
