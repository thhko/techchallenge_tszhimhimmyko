﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EasyBet.API.Configurations;
using EasyBet.API.Logics;
using Microsoft.AspNetCore.Mvc;

namespace EasyBet.API.Controllers
{
    [Route(ApiRouteConfig.ControllerRoute)]
    [Produces(ApiRouteConfig.ControllerProduces)]
    public class RaceController : Controller
    {
        private RaceLogics _raceLogics;

        public RaceController(RaceLogics raceLogics)
        {
            _raceLogics = raceLogics;
        }

        [HttpGet]
        public object GetAllRaces()
        {
            return _raceLogics.GetRaces();
        }

    }
}
