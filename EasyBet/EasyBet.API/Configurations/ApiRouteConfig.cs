﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EasyBet.API.Configurations
{
    public class ApiRouteConfig
    {
        public const string ControllerRoute = "api/[controller]";

        public const string ControllerProduces = "application/json";
    }
}
