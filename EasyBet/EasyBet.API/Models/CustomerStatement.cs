﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EasyBet.API.Models
{
    public class CustomerStatement
    {
        private long _customerId;
        private string _customerName;
        private decimal _totalBet = 0;
        private int _betCount = 0;

        public CustomerStatement(long customerId, string customerName)
        {
            _customerId = customerId;
            _customerName = customerName;
        }

        public void AddBet(decimal betAmount)
        {
            _totalBet += betAmount;
            _betCount++;
        }

        public long CustomerId => _customerId;
        public string CustomerName => _customerName;
        public long TotalBetCount => _betCount;
        public decimal TotalBetAmount => _totalBet;
        public bool IsRisky => _totalBet > 200;
    }
}
