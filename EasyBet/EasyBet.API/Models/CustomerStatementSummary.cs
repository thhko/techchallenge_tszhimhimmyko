﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EasyBet.API.Models
{
    public class CustomerStatementSummary
    {
        private List<CustomerStatement> _customerStatments = new List<CustomerStatement>();

        public void AddCustomerStatement(CustomerStatement customerStatement)
        {
            _customerStatments.Add(customerStatement);
        }

        public decimal TotalBetsValue => _customerStatments.Sum(c => c.TotalBetAmount);
        public List<CustomerStatement> CustomerStatements => _customerStatments;
    }
}
