﻿using Autofac;
using EasyBet.API.Logics;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace EasyBet.API
{
    public class AutofacModule : Autofac.Module
    {
        private IConfiguration _config;

        public AutofacModule(IConfiguration config)
        {
            _config = config;
        }

        protected override void Load(ContainerBuilder builder)
        {
            RegisterLogics(builder);
            RegisterServices(builder);
        }

        private void RegisterLogics(ContainerBuilder builder)
        {
            builder.RegisterType<BetLogics>();
            builder.RegisterType<CustomerLogics>();
            builder.RegisterType<RaceLogics>();
        }

        private void RegisterServices(ContainerBuilder builder)
        {
            System.Reflection.Assembly servicesAssembly = System.Reflection.Assembly.Load("EasyBet.API.Services");
            builder.RegisterAssemblyModules(servicesAssembly);
        }
    }
}
