﻿using System;
using System.Collections.Generic;
using EasyBet.UI.Models;
using EasyBet.UI.Models.Racers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EasyBet.UnitTests
{
    [TestClass]
    public class RaceTests
    {
        [TestMethod]
        public void Race_TotalBetsAmount_Test_Empty()
        {
            //arrange
            decimal expected = 0;
            Race<IRacer> race = new Race<IRacer>(RaceStatus.Completed, new List<IRacer> { });

            //act
            decimal totalBetsAmount = race.TotalBetsAmount;

            //assert
            Assert.AreEqual(expected, totalBetsAmount);
        }

        [TestMethod]
        public void Race_TotalBetsAmount_Test1()
        {
            //arrange
            decimal expected = 4000;
            IRacer racer1 = new Horse("Horse 1", 1, 2500, 10);
            IRacer racer2 = new Horse("Horse 2", 1, 1000, 10);
            IRacer racer3 = new Horse("Horse 3", 1, 500, 10);
            Race<IRacer> race = new Race<IRacer>(RaceStatus.Completed, new List<IRacer> { racer1, racer2, racer3 });

            //act
            decimal totalBetsAmount = race.TotalBetsAmount;

            //assert
            Assert.AreEqual(expected, totalBetsAmount);
        }

        [TestMethod]
        public void Race_TotalBetsAmount_Test_Decimals()
        {
            //arrange
            decimal expected = (decimal) 10.45;
            IRacer racer1 = new Horse("Horse 1", 1, (decimal)2.5, 10);
            IRacer racer2 = new Horse("Horse 2", 1, (decimal) 7.95, 10);
            Race<IRacer> race = new Race<IRacer>(RaceStatus.Completed, new List<IRacer> { racer1, racer2 });

            //act
            decimal totalBetsAmount = race.TotalBetsAmount;

            //assert
            Assert.AreEqual(expected, totalBetsAmount);
        }

        [TestMethod]
        [ExpectedException(typeof(OverflowException))]
        public void Race_TotalBetsAmount_Test_Decimals_Overflow()
        {
            //arrange
            IRacer racer1 = new Horse("Horse 1", 1, decimal.MaxValue, 10);
            IRacer racer2 = new Horse("Horse 2", 1, (decimal) 1, 10);
            Race<IRacer> race = new Race<IRacer>(RaceStatus.Completed, new List<IRacer> { racer1, racer2 });

            //act
            decimal totalBetsAmount = race.TotalBetsAmount;
        }

    }
}
