﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;

namespace EasyBet.Common
{
    public class HttpClient
    {
        private ManualResetEvent allDone;

        public HttpClient()
        {
            allDone = new ManualResetEvent(false);
        }

        #region HTTP call methods

        public bool Get(Uri URI, int waitTimeMs, out string responseMsg)
        {
            System.Net.WebRequest webRequest = System.Net.WebRequest.Create(URI);
            webRequest.Method = "GET";

            // Do the get and get the response.
            RequestState requestState = new RequestState();
            requestState.Request = webRequest;
            IAsyncResult asyncResult = webRequest.BeginGetResponse(new AsyncCallback(ResponseCallback), requestState);

            //wait for 1 set
            bool waitSignaled = allDone.WaitOne(waitTimeMs);

            if (waitSignaled)
            {
                responseMsg = requestState.ResponseMessage;
                return requestState.RequestSucceed;
            }
            else
            {
                responseMsg = JsonConvert.SerializeObject(new { status = 599, errors = new List<string> { "Request time out" } });
                return false;
            }

        }

        #endregion

        private void ResponseCallback(IAsyncResult result)
        {
            RequestState state = (RequestState)result.AsyncState;
            try
            {
                // Get and fill the RequestState
                WebRequest request = state.Request;
                // End the Asynchronous response and get the actual resonse object
                state.Response = (HttpWebResponse)request.EndGetResponse(result);
                Stream responseStream = state.Response.GetResponseStream();             

                System.IO.StreamReader sr = new System.IO.StreamReader(responseStream);
                state.ResponseMessage = sr.ReadToEnd().Trim();
                state.RequestSucceed = true;
            }
            catch (WebException wex)
            {
                state.RequestSucceed = false;

                HttpWebResponse httpResponse = wex.Response as HttpWebResponse;

                switch (httpResponse.StatusCode)
                {
                    case HttpStatusCode.Unauthorized:
                        state.ResponseMessage = "Your crendentials returned an unauthorized access (401)";
                        break;
                    case HttpStatusCode.Forbidden:
                        state.ResponseMessage = "Your crendentials returned an forbidden access (403)";
                        break;
                    default:
                        state.ResponseMessage = new System.IO.StreamReader(wex.Response.GetResponseStream()).ReadToEnd();
                        break;
                }

            }
            catch (Exception ex)
            {
                // Error handling
                state.RequestSucceed = false;
                state.ResponseMessage = ex.Message + "\r\n" + ex.InnerException + "\r\n" + ex.StackTrace;
            }

            allDone.Set();
        }

        private void RequestPost(IAsyncResult result)
        {
            RequestState state = (RequestState)result.AsyncState;

            Stream postStream = state.Request.EndGetRequestStream(result);

            string postData = state.RequestPostParameters;

            // Convert the string into a byte array.
            byte[] byteArray = Encoding.UTF8.GetBytes(postData);
            // Write to the request stream.
            postStream.Write(byteArray, 0, byteArray.Length);
            //postStream.Close();

            // Do the get and get the response.
            IAsyncResult asyncResult = state.Request.BeginGetResponse(new AsyncCallback(ResponseCallback), state);
            allDone.WaitOne();
        }

    }

    public class RequestState
    {
        public int BufferSize { get; private set; }
        public byte[] BufferRead { get; set; }
        public WebRequest Request { get; set; }
        public string RequestPostParameters { get; set; }
        public HttpWebResponse Response { get; set; }
        public Stream ResponseStream { get; set; }

        public bool RequestSucceed { get; set; }
        public string ResponseMessage { get; set; }

        public RequestState()
        {
            BufferSize = 1024;
            BufferRead = new byte[BufferSize];
            Request = null;
            ResponseStream = null;
        }
    }
}
