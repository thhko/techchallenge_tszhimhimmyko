﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace EasyBet.UI.Connector
{
    public class AutofacModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            RegisterConnectors(builder);
        }

        private void RegisterConnectors(ContainerBuilder builder)
        {
            builder.RegisterType<BetConnector>().AsImplementedInterfaces();
            builder.RegisterType<CustomerConnector>().AsImplementedInterfaces();
            builder.RegisterType<RaceConnector>().AsImplementedInterfaces();
            builder.RegisterType<CustomerStatementConnector>().AsImplementedInterfaces();
        }

    }
}
