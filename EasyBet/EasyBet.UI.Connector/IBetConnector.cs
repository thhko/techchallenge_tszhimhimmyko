﻿using EasyBet.UI.Connector.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace EasyBet.UI.Connector
{
    public interface IBetConnector
    {
        ApiResponse GetBets();
    }
}
