﻿using EasyBet.UI.Connector.Models;
using System;

namespace EasyBet.UI.Connector
{
    public interface IRaceConnector
    {
        ApiResponse GetRaces();
    }
}
