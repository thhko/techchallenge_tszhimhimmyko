﻿using EasyBet.Common;
using EasyBet.UI.Connector.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Text;

namespace EasyBet.UI.Connector
{
    public class BetConnector : BaseConnector, IBetConnector
    {
        private HttpClient _httpClient = new HttpClient();
        
        public ApiResponse GetBets()
        {
            Uri apiUri = new Uri(_ApiEndPoint + "/api/Bet");
            bool success = _httpClient.Get(apiUri, _ttl, out string responseMsg);

            ApiResponse response = new ApiResponse();
            if (success)
            {
                //read the response from API call
                dynamic apiData = JsonConvert.DeserializeObject<ExpandoObject>(responseMsg);
                if (apiData.success)
                    response.SuccessResponse(apiData.responseData);
                else
                    response.FailureResponse(apiData.responseData);
            }
            else
                response.FailureResponse(responseMsg);

             return response;
        }
    }
}
