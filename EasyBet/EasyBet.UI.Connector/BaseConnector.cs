﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;

namespace EasyBet.UI.Connector
{
    public class BaseConnector
    {
        public string _ApiEndPoint => ConfigurationManager.AppSettings["TargetEndPoint"];
        public int _ttl => 1500; //15secs
    }
}
