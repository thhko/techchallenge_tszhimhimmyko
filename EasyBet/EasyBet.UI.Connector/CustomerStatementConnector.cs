﻿using EasyBet.Common;
using EasyBet.UI.Connector.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace EasyBet.UI.Connector
{
    public class CustomerStatementConnector : BaseConnector, ICustomerStatementConnector
    {
        private HttpClient _httpClient = new HttpClient();

        public ApiResponse GetCustomersStatement()
        {
            Uri apiUri = new Uri(_ApiEndPoint + "/api/CustomerStatement");
            bool success = _httpClient.Get(apiUri, _ttl, out string responseMsg);

            ApiResponse response = new ApiResponse();
            if (success)
                response.SuccessResponse(responseMsg);
            else
                response.FailureResponse(responseMsg);

            return response;
        }

    }
}
