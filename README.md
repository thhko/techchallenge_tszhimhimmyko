# techchallenge_tszhimhimmyko

##Overview
This project provides a working solution to a horse racing betting system specificly targeting a delivery of internal useful information to the end users.

##Required Speicifications
* Functional website which displays information on horse races (Information to be displayed: Status of the race, total money placed on this race and each race participants' name, total bets placed on this race participant and the amount required to be paid out if this race participant won the race.)
* Functional Web API to be consumed by the website for race information
* Must connect to the provided end points for data (Reference to section "Provided Endpoints")
* Additional functional Web API end point that provides per customer their total bet counts, total bet amounts, a risk indicator of High (total bet amounts over $200) / Low (total bet amounts below or equal $200). And a total value of bets for all customers.

##Considerations
* The end points provided has limited number of results
* The project will be extended to other types of races

##Project Structure
This solution consists of two main components:  
####1. Website
This is the consumer of the WebAPI and it is solely communicating to the WebAPI for information required for it's purpose.    
####2. WebAPI - RESTful
Provides all of the required data relatd to the functional requirements specified.    

Both of these are designed for flexibility and extensibility by utilizing dependency injections coupled with Domain Driven Development.
Each of the projects are built with a 3-tier design for clear separation of concerns and high cohesion and are described as follows:

### Tier 1 - Controller
This tier handles the routing of the requests on both the website and web API.

### Tier 2 - Business Logics
The business logics tier is where the domain expert main involvements. This tier provides the fundamental processes for business requirements and is responsibile to communicate to the final tier of the project, which is the data access tier.

### Tier 3 - Data Layer
The final tier of the projects connects to the data end points. With dependency injection built within this layer, accessing different data sources can easily be achieved.
 
##Usage / Deployment
###Website
####_Specifications_  
.NET Framework 4.7.1 with MVC 5. You must ensure the environment for hosting/runnig this project to have .NET Framework 4.7.1 and MVC 5 installed.  
####_Configurations_  
You must also specify the API end point target location into the web.config:  
    <add key="TargetEndPoint" value="{endpoint_protocol}://{endpoint}"/>

The race information is currently displayed on the home page.
Example: https://myraceinformation.com/

### Web API	
####_Specifications_  
.NET Core 2.0 with WebAPI. You must ensure the environment for hosting/runnig this project to have .NET Core 2.0 is installed.  
####_Configurations_  
There are no speicifc configurations required for this project.    
	
####_API end points_
* /api/Bet  
Retrieves the full list of bets placed on races  
* /api/Customer  
Retrieves the full list of customers  
* /api/CustomerStatement  
Retreives the customer statement for all customers  
* /api/Race  
Retreives the full list of race information  
	
##Improvements
* Current solution deserialized all data returned from API end points into memory. This method is not ideal for large amount of data sets. An alternative processing must be implemented for larger data sets.
* API will require improvement
* Provide the Web API documentations into tools like Swagger
* Move race information access page in the Website to a more meaningful URL location
* Implement Website error handling for connecting to the Web API 

##Provided Endpoints
The following end points are currently used as the data sources.  
Race data: https://whatech-customerbets.azurewebsites.net/api/GetRaces?name=tszhimhimmyko  
Bets data: https://whatech-customerbets.azurewebsites.net/api/GetBetsV2?name=tszhimhimmyko  
Customers data: https://whatech-customerbets.azurewebsites.net/api/GetCustomers?name=tszhimhimmyko  